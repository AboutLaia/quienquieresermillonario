package com.example.salva.quienquieresermillonario.Databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Laia and Salva.
 */
public class ScoresSqlHelper extends SQLiteOpenHelper{

    public ScoresSqlHelper(Context context) {
        super(context, "database", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Scores (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                "name TEXT NOT NULL, score INTEGER NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Metodo para añadir una nueva puntuación a la base de datos
    //Se le pasan como parametros el nombre de usuario y su puntuacion
    public void addScore(String name, String score) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contenido = new ContentValues();
        //Guardamos en el contenido el nombre de usuario y su puntuacion
        contenido.put("name", name);
        contenido.put("score", score);
        //Insertamos en la tabla Scores
        db.insert("Scores", null, contenido);
        db.close();
    }

    //Metodo para borrar todas las puntuaciones de la tabla Scores
    public void clearAllScores() {
        SQLiteDatabase db = getWritableDatabase();
        //Borramos la tabla entera
        db.delete("Scores", null, null);
        db.close();
    }

    //Metodo para recuperar las puntuaciones del usuario
    public ArrayList<HashMap<String, String>> getScores() {
        ArrayList<HashMap<String, String>> res = new ArrayList<>();
        HashMap<String, String> hmap;

        SQLiteDatabase dbe = getReadableDatabase();
        Cursor cursor = dbe.query("Scores", new String[]{"name", "score"}, null, null, null, null, null);

        //Mientras haya resultados de la query
        while (cursor.moveToNext()){

            hmap = new HashMap<>();
            //Guardar el nombre de usuario
            hmap.put("name", cursor.getString(0));
            //Guardar la puntuacion
            hmap.put("score", cursor.getString(1));
            //Por ultimo, lo añadimos al resultado
            res.add(hmap);
        }
        cursor.close();
        dbe.close();
        return res;
    }

    private static ScoresSqlHelper ins;
    public synchronized static ScoresSqlHelper getIns(Context context) {
        if (ins == null) {
            ins = new ScoresSqlHelper(context.getApplicationContext());
        }
        return ins;
    }

}
