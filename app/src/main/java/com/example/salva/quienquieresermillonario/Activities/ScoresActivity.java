package com.example.salva.quienquieresermillonario.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TabHost;

import com.example.salva.quienquieresermillonario.Databases.ScoresSqlHelper;
import com.example.salva.quienquieresermillonario.R;
import com.example.salva.quienquieresermillonario.Task.GetPuntAsync;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScoresActivity extends AppCompatActivity {

    SharedPreferences preferences;
    TabHost thost;
    Context context;
    List<HashMap<String, String>> hashMapLocal;
    int current,next;
    SimpleAdapter Localadapter, Friendsadapter;
    GetPuntAsync puntuaciones;
    boolean delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);
        context=this;

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Creación de los TabHost
        thost = (TabHost) findViewById(R.id.tabHost);
        thost.setup();
        TabHost.TabSpec spec = thost.newTabSpec(getString(R.string.tab_local));

        //Pestaña Local
        spec.setIndicator(getString(R.string.tab_local), getResources().getDrawable(R.drawable.scores));
        spec.setContent(R.id.tabLocal);
        thost.addTab(spec);

        //Pestaña Friends
        spec = thost.newTabSpec(getString(R.string.tab_friends));
        spec.setIndicator(getString(R.string.tab_friends), getResources().getDrawable(R.drawable.scores));
        spec.setContent(R.id.tabFriends);
        thost.addTab(spec);

        thost.setCurrentTab(preferences.getInt("tab_actual", 0));

        thost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                preferences = PreferenceManager.getDefaultSharedPreferences(ScoresActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                String local = getString(R.string.tab_local);

                //Comprobacion para saber si mostrar o no el icono de la papelera
                //En local si, en amigos no
                if (local.compareTo(tabId) == 0) {
                    editor.putBoolean("delete", true);
                } else {
                    editor.putBoolean("delete", false);
                }
                editor.apply();
                supportInvalidateOptionsMenu();
            }
        });

        hashMapLocal = new ArrayList<>();
        //Añadimos a la base de datos
        hashMapLocal.addAll(ScoresSqlHelper.getIns(this).getScores());
        HashMap<String,String> hmapaux;

        // Ordenamos en funcion de las puntuaciones
        for(int i = 0;i < hashMapLocal.size()-1;i++){
            for(int j=i+1;j<hashMapLocal.size();j++){
                current = Integer.parseInt(hashMapLocal.get(i).get("score"));
                next = Integer.parseInt(hashMapLocal.get(j).get("score"));
                if(current < next){
                    hmapaux = hashMapLocal.get(i);
                    hashMapLocal.set(i,hashMapLocal.get(j));
                    hashMapLocal.set(j,hmapaux);
                }
            }
        }

        // Asignamos el ListView de la fila al Adapter
        ListView local = (ListView) findViewById(R.id.tabLocal);
        Localadapter = new SimpleAdapter(this,hashMapLocal,R.layout.score_row,new String[]{"name","score"},new int[]{R.id.tvName_row,R.id.tvScore_row});
        local.setAdapter(Localadapter);

        //Comprobamos que tengamos conectividad
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo infonet = manager.getActiveNetworkInfo();
        if(infonet.isConnected() && infonet != null ){
            puntuaciones = new GetPuntAsync();
            puntuaciones.setParent(this);
            String [] params = {preferences.getString("username", "No Name")};
            puntuaciones.execute(params);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tab_actual", thost.getCurrentTab());
        editor.apply();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Crear el icono de borrar todo (papelera)
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        preferences = PreferenceManager.getDefaultSharedPreferences(ScoresActivity.this);
        delete = preferences.getBoolean("delete", true);
        menu.findItem(R.id.delete_all).setVisible(delete);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            // Botón de la papelera(borrar todo)
            case R.id.delete_all:
                //Creamos una ventana de alerta
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.title_delete_all);
                builder.setMessage(R.string.text_delete_all);

                //NO
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                //SI
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Si pulsa que si, borramos todas las puntuaciones de la base de datos
                        ScoresSqlHelper.getIns(context).clearAllScores();
                        hashMapLocal.clear();
                        Localadapter.notifyDataSetChanged();
                        //supportInvalidateOptionsMenu();
                    }
                });
                builder.create().show();
                return true;
        }
        // Se vuelve a lanzar MainActivity porque al pulsar un botón en MainActivity esta se destruye
        Intent intent = new Intent(ScoresActivity.this, MainActivity.class);
        startActivity(intent);
        ScoresActivity.this.finish();
        supportInvalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }

    //Metodo para mostrar las puntuaciones de los amigos
    public void gotScores(List<HashMap<String, String>> hashMapListFriends) {
        // Adaptar para la pestaña de amigos
        ListView friends = (ListView) findViewById(R.id.tabFriends);
        Friendsadapter = new SimpleAdapter(this,hashMapListFriends,R.layout.score_row,new String[]{"name","score"},new int[]{R.id.tvName_row,R.id.tvScore_row});
        friends.setAdapter(Friendsadapter);
    }
}
