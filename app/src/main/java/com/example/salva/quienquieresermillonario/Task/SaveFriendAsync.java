package com.example.salva.quienquieresermillonario.Task;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.salva.quienquieresermillonario.Activities.SettingsActivity;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Laia and Salva.
 */
public class SaveFriendAsync extends AsyncTask<String, Void, Void>{

    SettingsActivity parent = null;

    @Override
    protected Void doInBackground(String... parameters) {
        HttpURLConnection connect = null;
        Uri.Builder build = new Uri.Builder();
        //Construimos la URL
        build.scheme("http");
        build.authority("wwtbamandroid.appspot.com");
        build.appendPath("rest");
        build.appendPath("friends");
        //Añadimos el nombre del jugador junto con su puntuacion
        String rest = "name="+parameters[0]+"&friend_name="+parameters[1];
        Log.i("Body", rest);

        try {
            URL url = new URL(build.build().toString());
            //Abrimos conexion http
            connect = (HttpURLConnection) url.openConnection();
            //Metodo escritura
            connect.setRequestMethod("POST");
            connect.setDoInput(true);
            connect.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connect.getOutputStream());
            writer.write(rest);
            //Close ya invoca a flush
            writer.close();
            //Obligatorio siempre aunque no se espere nada
            InputStreamReader inreader = new InputStreamReader(connect.getInputStream());
            inreader.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connect.disconnect();
        }
        return null;
    }

    public void setParent(SettingsActivity parent) {
        this.parent = parent;
    }
}
