package com.example.salva.quienquieresermillonario.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.salva.quienquieresermillonario.Databases.ScoresSqlHelper;
import com.example.salva.quienquieresermillonario.R;
import com.example.salva.quienquieresermillonario.Task.PutPuntAsync;
import com.example.salva.quienquieresermillonario.pojo.Question;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PlayActivity extends AppCompatActivity {

    //Variables que vamos a necesitar
    //Numero de preguntas
    int nquestions=15;
    //Fin de la partida: saber cuando acaba una partida
    boolean finpartida=false;
    //Comprobar cuando se pulsa el comodin del publico
    boolean baudience;
    //Comprobar cuando se pulsa el comodin de la llamada
    boolean bphone;
    //Comprobar cuando se pulsa el comodin del 50%
    boolean fifty;
    //Guardar las preguntas
    ArrayList<Question> questions;
    // Variable para almacenar el premio
    int cact=0;
    //Lista de precios
    Integer[] price = {100,200,300,500,1000,2000,4000,8000,16000,32000,64000,125000,250000,500000,1000000};
    //Botones para asociar al texto que se muestra en la interfaz
    Button banswer1;
    Button banswer2;
    Button banswer3;
    Button banswer4;
    //texto de la cantidad por la que se juega
    TextView payfor;
    //Numero de la pregunta actual
    TextView nquestion;
    //Texto de la pregunta
    TextView tvtext;
    //Posicion de la informacion de la pregunta en cada momento
    //Iniciamos a 0->primera pregunta
    int indice=0;
    String answer1, answer2, answer3, answer4, fifty1, fifty2, audience, number, phone, right, text;

    //Variable compartida
    SharedPreferences preferences;
    //Objeto para guardar puntuaciones
    PutPuntAsync puntuacion;

    //Variables para comprobar si deben aparecer los botones (50%)
    int vanswer1, vanswer2, vanswer3, vanswer4;

    //Variable por defecto para los comodines
    int nayudas;

    //PAra saber si hace falta actualizar la action bar
    boolean action_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        questions=new ArrayList<>();

        //Tarea para leer el xml,
        //controlar que comodines mostrar i
        //que botones mostrar
        GetQuestionTask();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_50, menu);
        getMenuInflater().inflate(R.menu.menu_publico, menu);
        getMenuInflater().inflate(R.menu.menu_telefono, menu);
        getMenuInflater().inflate(R.menu.menu_close, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Modificar la visibilidad de los botones del Action Bar
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        bphone = preferences.getBoolean("phone", true);
        fifty = preferences.getBoolean("fifty", true);
        baudience = preferences.getBoolean("audience", true);
        menu.findItem(R.id.action_itelefono).setVisible(bphone);
        menu.findItem(R.id.action_i50).setVisible(fifty);
        menu.findItem(R.id.action_iaudience).setVisible(baudience);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        String fifty1, fifty2;

        //Editor de preferencias para quitar botones al pulsarlos
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        // Determine the action to take place according to the Id of the action selected
        switch (item.getItemId()) {

            case R.id.action_i50:
                fifty1=questions.get(indice).getFifty1();
                fifty2=questions.get(indice).getFifty2();

                //Si alguna de las dos opciones a quitar es el primer boton
                if(fifty1.equals("1") || fifty2.equals("1")){
                    editor.putInt("vanswer1", View.INVISIBLE);
                    editor.apply();
                    banswer1.setVisibility(View.INVISIBLE);
                }

                //Si alguna de las dos opciones a quitar es el segundo boton
                if(fifty1.equals("2") || fifty2.equals("2")){
                    editor.putInt("vanswer2", View.INVISIBLE);
                    editor.apply();
                    banswer2.setVisibility(View.INVISIBLE);
                }

                //Si alguna de las dos opciones a quitar es el tercer boton
                if(fifty1.equals("3") || fifty2.equals("3")){
                    editor.putInt("vanswer3", View.INVISIBLE);
                    editor.apply();
                    banswer3.setVisibility(View.INVISIBLE);
                }

                //Si alguna de las dos opciones a quitar es el cuarto boton
                if(fifty1.equals("4") || fifty2.equals("4")){
                    editor.putInt("vanswer4", View.INVISIBLE);
                    editor.apply();
                    banswer4.setVisibility(View.INVISIBLE);
                }

                editor.putBoolean("fifty", false);
                editor.apply();
                //supportInvalidateOptionsMenu();
                supportInvalidateOptionsMenu();
                return true;

            case R.id.action_icredits:
                break;

            case R.id.action_itelefono:
                Toast.makeText(this, "Correct answer is: "+questions.get(indice).getPhone(), Toast.LENGTH_SHORT).show();
                editor.putBoolean("phone", false);
                editor.apply();
                //supportInvalidateOptionsMenu();
                supportInvalidateOptionsMenu();
                return true;

            case R.id.action_iclose:

                Toast.makeText(this, R.string.wrong, Toast.LENGTH_SHORT).show();
                editor.putBoolean("action_bar", true);
                editor.putInt("vanswer1", View.VISIBLE);
                editor.putInt("vanswer2", View.VISIBLE);
                editor.putInt("vanswer3", View.VISIBLE);
                editor.putInt("vanswer4", View.VISIBLE);
                editor.apply();
                StoreData();
                Intent i2 = new Intent(PlayActivity.this, MainActivity.class);
                startActivity(i2);
                PlayActivity.this.finish();

                return true;

            case R.id.action_iaudience:
                Toast.makeText(this, "Correct answer is: "+questions.get(indice).getAudience(), Toast.LENGTH_SHORT).show();
                editor.putBoolean("audience", false);
                editor.apply();
                //supportInvalidateOptionsMenu();
                supportInvalidateOptionsMenu();
                break;
        }

        if (intent != null) {
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public Void GetQuestionTask()  {
        try{

            InputStream fis= getResources().openRawResource(R.raw.questions);
            InputStreamReader reader = new InputStreamReader(fis);
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(reader);

            int eventType = parser.getEventType();
            Question question = null;

            while (XmlPullParser.END_DOCUMENT != eventType) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if ("question".equals(parser.getName())) {

                            answer1 = String.format(parser.getAttributeValue(null, "answer1"));
                            answer2 = String.format(parser.getAttributeValue(null, "answer2"));
                            answer3 = String.format(parser.getAttributeValue(null, "answer3"));
                            answer4 = String.format(parser.getAttributeValue(null, "answer4"));
                            audience = String.format(parser.getAttributeValue(null, "audience"));
                            fifty1 = String.format(parser.getAttributeValue(null, "fifty1"));
                            fifty2 = String.format(parser.getAttributeValue(null, "fifty2"));
                            number = String.format(parser.getAttributeValue(null, "number"));
                            phone = String.format(parser.getAttributeValue(null, "phone"));
                            right = String.format(parser.getAttributeValue(null, "right"));
                            text = String.format(parser.getAttributeValue(null, "text"));

                            question=new Question(number, text, answer1, answer2, answer3, answer4, right, audience, phone, fifty1, fifty2);
                            questions.add(question);

                        }

                        break;
                }

                parser.next();
                eventType = parser.getEventType();
            }

        }
        catch (XmlPullParserException e) {e.printStackTrace();}
        catch (FileNotFoundException e){e.printStackTrace();}
        catch (IOException e){e.printStackTrace();}


        payfor = (TextView) findViewById(R.id.tvmoney);
        nquestion = (TextView) findViewById(R.id.tvnquestion);
        tvtext = (TextView) findViewById(R.id.tvquestion);

        //Guardar en variables compartidas
        cact = preferences.getInt("cantidad_actual", 0);
        indice=preferences.getInt("indice", 0);

        //Indicar el numero de pregunta
        nquestion.setText(questions.get(indice).getNumber());

        banswer1=(Button) findViewById(R.id.banswer1);
        banswer2=(Button) findViewById(R.id.banswer2);
        banswer3=(Button) findViewById(R.id.banswer3);
        banswer4=(Button) findViewById(R.id.banswer4);

        banswer1.setText(questions.get(indice).getAnswer1());
        banswer2.setText(questions.get(indice).getAnswer2());
        banswer3.setText(questions.get(indice).getAnswer3());
        banswer4.setText(questions.get(indice).getAnswer4());

        tvtext.setText(questions.get(indice).getText());
        payfor.setText(price[indice].toString());

        //Guardar la visibilidad de los botones (al empezar pregunta todos visibles
        vanswer1=preferences.getInt("vanswer1", View.VISIBLE);
        vanswer2=preferences.getInt("vanswer2", View.VISIBLE);
        vanswer3=preferences.getInt("vanswer3", View.VISIBLE);
        vanswer4=preferences.getInt("vanswer4", View.VISIBLE);
        //Comprobamos si hay que mostrarlos todos
        if(vanswer1==0){
            banswer1.setVisibility(View.VISIBLE);
        }
        else{
            banswer1.setVisibility(View.INVISIBLE);
        }

        if(vanswer2==0){
            banswer2.setVisibility(View.VISIBLE);
        }
        else{
            banswer2.setVisibility(View.INVISIBLE);
        }

        if(vanswer3==0){
            banswer3.setVisibility(View.VISIBLE);
        }
        else{
            banswer3.setVisibility(View.INVISIBLE);
        }

        if(vanswer4==0){
            banswer4.setVisibility(View.VISIBLE);
        }
        else{
            banswer4.setVisibility(View.INVISIBLE);
        }


        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("finPartida", false);
        editor.apply();

        //Controlar que comodines aparecen segun las preferencias
        //Por defecto se usan los 3 comodines
        nayudas =preferences.getInt("nayudas", 3);
        action_bar=preferences.getBoolean("action_bar", true);
        if (action_bar){
            switch (nayudas){
                case 3:
                    editor.putBoolean("phone", true);
                    editor.putBoolean("fifty", true);
                    editor.putBoolean("audience", true);
                    editor.apply();
                    break;
                case 2:
                    editor.putBoolean("phone", true);
                    editor.putBoolean("fifty", true);
                    editor.putBoolean("audience", false);
                    editor.apply();
                    break;
                case 1:
                    editor.putBoolean("phone", true);
                    editor.putBoolean("fifty", false);
                    editor.putBoolean("audience", false);
                    editor.apply();
                    break;
                case 0:
                    editor.putBoolean("phone", false);
                    editor.putBoolean("fifty", false);
                    editor.putBoolean("audience", false);
                    editor.apply();
                    Log.i("Call al onCreate:", "" + preferences.getBoolean("phone", true));
                    break;
                default:
                    editor.putBoolean("phone", false);
                    editor.putBoolean("fifty", false);
                    editor.putBoolean("audience", false);
                    editor.apply();
                    break;


            }
            editor.putBoolean("action_bar", false);
            editor.apply();
        }
        return null;
    }

    @Override
    protected void onStop() {
        super.onStop();
        payfor = (TextView) findViewById(R.id.tvmoney);
        nquestion = (TextView) findViewById(R.id.tvnquestion);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("cantidad_actual", cact);
        editor.putInt("vanswer1", preferences.getInt("vanswer1", View.VISIBLE));
        editor.putInt("vanswer2", preferences.getInt("vanswer2", View.VISIBLE));
        editor.putInt("vanswer3", preferences.getInt("vanswer3", View.VISIBLE));
        editor.putInt("vanswer4", preferences.getInt("vanswer4", View.VISIBLE));

        finpartida=preferences.getBoolean("finPartida", false);
        if(finpartida == false){
            //Guardar indice si no es el final de la partida
            editor.putInt("indice", indice);
            editor.putBoolean("action_bar", false);
        }
        else {
            //Si se acaba la partida, el indice vuelve al principio
            editor.putInt("indice", 0);
            //Se debe actualizar la action bar
            editor.putBoolean("action_bar", true);
        }
        editor.apply();
    }


    //Metodo para guardar los datos
    //1. En la base de datos
    //2.En el servidor rest
    public void StoreData(){
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("finPartida", true);
        editor.apply();

        //Guardar en la base de datos
        ScoresSqlHelper.getIns(this).addScore(preferences.getString("username", "No Name"), Integer.toString(cact));

        //Guardar en el servidor rest
        //Debemos comprobar si hay conexion a internet
        ConnectivityManager cmanager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo infonet = cmanager.getActiveNetworkInfo();

        if(infonet.isConnected() && infonet!=null){
            preferences = PreferenceManager.getDefaultSharedPreferences(this);
            puntuacion = new PutPuntAsync();
            puntuacion.setParent(this);
            if(cact >= 1000 && cact < 32000){
                cact = 1000;
            }
            else if(cact >= 32000){
                cact = 32000;
            }
            else{
                cact = 0;
            }
            Object [] parametros = {preferences.getString("username", "No Name"),cact};
            puntuacion.execute(parametros);

        }


    }

    //Metodo para comprobar que se pulsa el boton correcto
    //Si es asi, se incrementa indice para cambiar de pregunta
    //Sino, se invoca al metodo EndGame para matar la actividad
    public void CheckAnswer (View v){
        String correcta = questions.get(indice).getRight();
        switch (v.getId()){
            case R.id.banswer1:
                if(correcta.compareTo("1") == 0) {
                    if (comprobarIndice() == false) {
                        indice++;
                        Toast.makeText(this, R.string.right_answer, Toast.LENGTH_SHORT).show();
                        cact=price[indice];
                        RefreshData();
                    } else {
                        EndGame();
                        cact=0;
                    }
                }
                else{
                    WrongAnswer();
                }
                break;

            case R.id.banswer2:
                if(correcta.compareTo("2") == 0){
                    if (comprobarIndice() == false) {
                        indice++;
                        Toast.makeText(this, R.string.right_answer, Toast.LENGTH_SHORT).show();
                        cact=price[indice];
                        RefreshData();
                    } else {
                        EndGame();
                        cact=0;
                    }
                }
                else{
                    WrongAnswer();
                }
                break;

            case R.id.banswer3:
                if(correcta.compareTo("3") == 0){
                    if (comprobarIndice() == false) {
                        indice++;
                        Toast.makeText(this, R.string.right_answer, Toast.LENGTH_SHORT).show();
                        cact=price[indice];
                        RefreshData();
                    } else {
                        EndGame();
                        cact=0;
                    }
                }
                else{
                    WrongAnswer();
                }
                break;

            case R.id.banswer4:
                if(correcta.compareTo("4") == 0){
                    if (comprobarIndice() == false) {
                        indice++;
                        Toast.makeText(this, R.string.right_answer, Toast.LENGTH_SHORT).show();
                        cact=price[indice];
                        RefreshData();
                    } else {
                        EndGame();
                        cact=0;
                    }
                }
                else{
                    WrongAnswer();
                }
                break;
        }
    }

    // Metodo para comprobar si hemos llegado a la pregunta final
    public boolean comprobarIndice(){
        if(indice>=nquestions-1){
            return true;
        }
        else{
            return false;
        }
    }

    //Metodo para refrescar toda la información de la actividad
    public void RefreshData(){

        banswer1 = (Button) findViewById(R.id.banswer1);
        banswer2 = (Button) findViewById(R.id.banswer2);
        banswer3 = (Button) findViewById(R.id.banswer3);
        banswer4 = (Button) findViewById(R.id.banswer4);
        tvtext = (TextView) findViewById(R.id.tvquestion);
        payfor = (TextView) findViewById(R.id.tvmoney);
        nquestion = (TextView) findViewById(R.id.tvnquestion);

        //Reestablecer visibilidad de los botones
        banswer1.setVisibility(View.VISIBLE);
        banswer2.setVisibility(View.VISIBLE);
        banswer3.setVisibility(View.VISIBLE);
        banswer4.setVisibility(View.VISIBLE);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("vanswer1", View.VISIBLE);
        editor.putInt("vanswer2", View.VISIBLE);
        editor.putInt("vanswer3", View.VISIBLE);
        editor.putInt("vanswer4", View.VISIBLE);
        editor.apply();

        //Mostrar nueva informacion de cada elemento
        banswer1.setText(questions.get(indice).getAnswer1());
        banswer2.setText(questions.get(indice).getAnswer2());
        banswer3.setText(questions.get(indice).getAnswer3());
        banswer4.setText(questions.get(indice).getAnswer4());
        tvtext.setText(questions.get(indice).getText());
        payfor.setText(price[indice].toString());
        nquestion.setText(questions.get(indice).getNumber());
    }

    //Metodo para informar al usuario de que se ha equivocado
    //Y reestablece la configuracion
    public void WrongAnswer(){

        if(cact >= 1000 && cact < 32000){
            cact = 1000;
        }
        else if(cact >= 32000){
            cact = 32000;
        }
        else{
            cact = 0;
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("finPartida", true);
        editor.putBoolean("action_bar", true);
        editor.putInt("vanswer1", View.VISIBLE);
        editor.putInt("vanswer2", View.VISIBLE);
        editor.putInt("vanswer3", View.VISIBLE);
        editor.putInt("vanswer4", View.VISIBLE);
        editor.apply();

        Toast.makeText(this, getString(R.string.wrong_answer)+"\n"+getString(R.string.money_won)+cact, Toast.LENGTH_SHORT).show();


        StoreData();
        Intent intent = new Intent(PlayActivity.this, MainActivity.class);
        startActivity(intent);
        PlayActivity.this.finish();

    }

    //Metodo para cuando finaliza el juego (acierta hasta la ultima pregunta)
    public void EndGame(){
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("finalPartida", true);
        editor.putBoolean("action_bar", true);
        editor.putInt("ans1_vis", View.VISIBLE);
        editor.putInt("ans2_vis", View.VISIBLE);
        editor.putInt("ans3_vis", View.VISIBLE);
        editor.putInt("ans4_vis", View.VISIBLE);
        editor.apply();

        Toast.makeText(this, R.string.congratulations, Toast.LENGTH_SHORT).show();
        StoreData();
        Intent intent = new Intent(PlayActivity.this, MainActivity.class);
        startActivity(intent);
        PlayActivity.this.finish();



    }
}
