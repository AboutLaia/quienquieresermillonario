package com.example.salva.quienquieresermillonario.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.salva.quienquieresermillonario.R;
import com.example.salva.quienquieresermillonario.Task.PutPuntAsync;
import com.example.salva.quienquieresermillonario.Task.SaveFriendAsync;

public class SettingsActivity extends AppCompatActivity {


    Spinner spinnerJoker;
    SharedPreferences preferences;
    EditText editTextName, editTextFriend;
    SaveFriendAsync puntuacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Cogemos los elementos que vamos a utilizar
        editTextName = (EditText) findViewById(R.id.etUserName);
        spinnerJoker = (Spinner) findViewById(R.id.spinnerJoker);
        editTextFriend = (EditText) findViewById(R.id.etNameFriend);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editTextName.setText(preferences.getString("username", ""));
        spinnerJoker.setSelection(preferences.getInt("nayudas", 0));

    }

    @Override
    protected void onPause() {
        editTextName = (EditText) findViewById(R.id.etUserName);
        spinnerJoker = (Spinner) findViewById(R.id.spinnerJoker);
        // Guardar el nombre de usuario y el número de ayudas
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", editTextName.getText().toString());
        editor.putInt("nayudas", spinnerJoker.getSelectedItemPosition());
        editor.apply();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
        startActivity(intent);
        SettingsActivity.this.finish();
        supportInvalidateOptionsMenu();
        return super.onOptionsItemSelected(item);
    }


    public void addFriend(View v) {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo infonet = manager.getActiveNetworkInfo();
        if ( infonet.isConnected() && infonet != null) {
            preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String[] params = {preferences.getString("username", "No Name"), editTextFriend.getText().toString()};
            puntuacion = new SaveFriendAsync();
            puntuacion.setParent(this);
            puntuacion.execute(params);
            editTextFriend = (EditText) findViewById(R.id.etNameFriend);
            editTextFriend.setText("");
            Toast.makeText(this, getText(R.string.friend_added), Toast.LENGTH_SHORT).show();
        }
    }
}
