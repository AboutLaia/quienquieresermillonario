package com.example.salva.quienquieresermillonario.Task;

import android.net.Uri;
import android.os.AsyncTask;

import com.example.salva.quienquieresermillonario.Activities.ScoresActivity;
import com.example.salva.quienquieresermillonario.pojo.HighScore;
import com.example.salva.quienquieresermillonario.pojo.HighScoreList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/**
 * Created by Laia and salva.
 */
public class GetPuntAsync extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {

    ArrayList<HashMap<String, String>> res;
    HighScoreList Scores;
    List<HighScore> lista;
    HashMap<String, String> hmap;
    int aux;
    int next;
    ScoresActivity parent = null;

    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(String... parameters) {
        InputStreamReader inputreader = null;
        HttpURLConnection connect = null;
        Uri.Builder build = new Uri.Builder();
        build.scheme("http");
        build.authority("wwtbamandroid.appspot.com");
        build.appendPath("rest");
        build.appendPath("highscores");
        build.appendQueryParameter("name", parameters[0]);

        try {
            URL url = new URL(build.build().toString());
            connect = (HttpURLConnection) url.openConnection();
            //Metodo lectura
            connect.setRequestMethod("GET");
            connect.setDoInput(true);
            if (connect.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputreader = new InputStreamReader(connect.getInputStream());
                //Creamos un objeto de clase Gson para convertir JSON en Java
                Gson gson = new Gson();
                try {
                    //Cogemos las puntuaciones
                    //fromJson: convierte de JSON a Java
                    Scores = gson.fromJson(inputreader, HighScoreList.class);
                } catch (JsonSyntaxException | JsonIOException e) {
                    e.printStackTrace();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                inputreader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connect.disconnect();
        }

        //Obtenemos las puntuaciones
        lista=Scores.getScores();
        res=new ArrayList<>();

        //Guardamos los amigos y sus puntuaciones en un HashMap
        for (int i = 0; i < lista.size(); i++) {
            // Comprobamos que no añadimos al propio usuario como amigo
            if(lista.get(i).getName().compareTo(parameters[0]) != 0) {
                hmap = new HashMap<>();
                hmap.put("name", lista.get(i).getName());
                hmap.put("score", lista.get(i).getScoring());
                res.add(hmap);
            }
        }

        //Ordenamos la lista de amigos segun sus puntuaciones
        HashMap<String,String> hmapaux;
        for(int i=0; i < res.size()-1; i++){
            for(int j=i+1; j<res.size(); j++){
                aux = Integer.parseInt(res.get(i).get("score"));
                next = Integer.parseInt(res.get(j).get("score"));
                if(aux < next){
                    hmapaux = res.get(i);
                    res.set(i,res.get(j));
                    res.set(j,hmapaux);
                }
            }
        }
        return res;
    }
    @Override
    protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
        if(result != null) {
            //ScoresActivity
            parent.gotScores(result);
        }
        super.onPostExecute(result);
    }

    public void setParent(ScoresActivity parent) {
        this.parent = parent;
    }

}
