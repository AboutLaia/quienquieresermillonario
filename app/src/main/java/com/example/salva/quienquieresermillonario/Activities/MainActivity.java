package com.example.salva.quienquieresermillonario.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.salva.quienquieresermillonario.R;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_credits, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        // Determine the action to take place according to the Id of the action selected
        switch (item.getItemId()) {

            case R.id.action_icredits:
                intent = new Intent(this, CreditsActivity.class);
                break;
        }

        if (intent != null) {
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /*
        This method is executed when any button in the Dashboard is clicked
    */
    public void mainButtonClicked(View v) {

        // Intent object to launch the related activity
        Intent intent = null;

        // Determine the activity to launch according to the Id of the Button clicked
        switch (v.getId()) {

            // Activity for getting a new quotation and adding it to favourites
            case R.id.bPlay:
                intent = new Intent(this, PlayActivity.class);
                break;

            // Activity for displaying and managing the favourite quotations
            case R.id.bScores:
                intent = new Intent(this, ScoresActivity.class);
                break;

            // Activity for configuring the application
            case R.id.bSettings:
                intent = new Intent(this, SettingsActivity.class);
                break;

        }

        // Launch the required activity
        if (intent != null) {
            startActivity(intent);
        }
    }
}
